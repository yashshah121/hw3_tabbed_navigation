﻿using Xamarin.Forms;

namespace TabbedPageWithNavigationPage
{
	public partial class MainPage : TabbedPage
	{
		public MainPage ()
		{
			InitializeComponent();
			//Following we have defined 4 tabs where we are setting Icon image here. Every player has their own unique Icon
			var Messi = new NavigationPage(new Messi());
			var Ronaldo = new NavigationPage(new Ronaldo());
			var Neymar= new NavigationPage(new Neymar());
			var Saurez = new NavigationPage(new Saurez());
			Messi.IconImageSource = "messi.png";
			Messi.Title = "Messi";
			Ronaldo.IconImageSource = "ronaldoicon.png";
			Ronaldo.Title = "Ronaldo";
			Neymar.IconImageSource = "neymaricon.png";
			Neymar.Title = "Neymar";
			Saurez.IconImageSource = "saurezicon.png";
			Saurez.Title = "Saurez";

			Children.Add(Messi);
			Children.Add(Ronaldo);
			Children.Add(Neymar);
			Children.Add(Saurez);
		}
	}
}

