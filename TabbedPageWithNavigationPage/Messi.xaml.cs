﻿using Xamarin.Forms;
using System;

namespace TabbedPageWithNavigationPage
{
	public partial class Messi : ContentPage
	{
		//In Xaml file First we define the title then we define using the stacklayout and scrollview.
		//we also insert images of the player and have button and it's onclick function we call webview file.
		public Messi()
		{
			InitializeComponent();
		}
		async void OnMessiVideoClicked (object sender, EventArgs e)
		{
			await Navigation.PushAsync(new MessiWebView());
		}
	}
}

