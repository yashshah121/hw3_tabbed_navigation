﻿using System;
using Xamarin.Forms;

namespace TabbedPageWithNavigationPage
{
	public partial class RonaldoWebView : ContentPage
	{
		public RonaldoWebView()
		{
			// Here we call there particular werbsite using the welcome message and the exit message.
			// I have set webview before the welcome message so that page loads faster.
			InitializeComponent();
		}
			protected async override void OnAppearing()
		{
			base.OnAppearing();
			var browser = new WebView();
			browser.Source = "https://www.cristianoronaldo.com/";
			Content = browser;
			await DisplayAlert("Welcome", "You are visiting Ronaldo", "Continue");



		}
		//This will give a alert message while poping back to the previous page.
		protected async override void OnDisappearing()
		{

			await DisplayAlert("Bye", "Thanks For Visting", "Exit");


		}
	}
}

