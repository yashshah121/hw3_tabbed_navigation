﻿using System;
using Xamarin.Forms;

namespace TabbedPageWithNavigationPage
{
	public partial class Ronaldo : ContentPage
	{
		public Ronaldo ()
		{
			//In Xaml file First we define the title then we define using the stacklayout and scrollview.
			//we also insert images of the player and have button and it's onclick function we call webview file.
			InitializeComponent();
		}

		async void OnRonaldoClicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new RonaldoWebView());
		}
	}
}

