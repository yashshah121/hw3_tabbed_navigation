﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TabbedPageWithNavigationPage
{
	public partial class Neymar : ContentPage
	{
		public Neymar ()
		{
			//In Xaml file First we define the title then we define using the stacklayout and scrollview.
			//we also insert images of the player and have button and it's onclick function we call webview file.
			InitializeComponent();

		}
		async void OnNeymarClicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new NeymarWebView());
		}
	}
}

