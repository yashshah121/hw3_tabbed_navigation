﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedPageWithNavigationPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MessiWebView : ContentPage
    {
        public MessiWebView()
        {
			// Here we call there particular werbsite using the welcome message and the exit message.
			// I have set webview before the welcome message so that page loads faster.
            InitializeComponent();
        }
		protected async override void OnAppearing()
		{
			base.OnAppearing();
			var browser = new WebView();
			browser.Source = "https://messi.com/en/biography/";
			Content = browser;
			await DisplayAlert("Welcome", "You are visiting Messi", "Continue");


		}
		//This will give a alert message while poping back to the previous page.
		protected async override void OnDisappearing()
		{

			await DisplayAlert("Bye", "Thanks for visiting", "Exit");


		}
	}
}