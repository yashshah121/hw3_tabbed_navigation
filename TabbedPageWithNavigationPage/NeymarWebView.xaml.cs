﻿using System;
using Xamarin.Forms;

namespace TabbedPageWithNavigationPage
{
	public partial class NeymarWebView : ContentPage
	{
		public NeymarWebView()
		{
			// Here we call there particular werbsite using the welcome message and the exit message.
			// I have set webview before the welcome message so that page loads faster.
			InitializeComponent();
		}
			protected async override void OnAppearing()
		{
			base.OnAppearing();
			var browser = new WebView();
			browser.Source = "https://www.neymarjr.com/en/";
			Content = browser;
			await DisplayAlert("welcome", "You are visiting Neymar", "Continue");



		}
		//This will give a alert message while poping back to the previous page.
		protected async override void OnDisappearing()
		{
			await DisplayAlert("Bye", "Thanks for visiting", "Exit");
		}
	}
}

